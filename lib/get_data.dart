
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class GetData {
  static Dio customClient =
      Dio()..interceptors.add(PrettyDioLogger(requestBody:true));

  static const String BASE_URL = "https://pokeapi.co/api/v2/pokemon/";
  //https://rest.inleggo.io/item/item.all.php
  static Future<String> fetchData(String pokemonId) async {
    String url = '$BASE_URL$pokemonId';
    Response response = await customClient.post(url,data:{
      'api-key':'123',
      'Authorization': 'token',
    });
    /*
    Response response = await customClient.post(
      url,
      data: {"server": "45", "tipo": "cod_inv_lasted", "code": "000001"},
      options: Options(
        headers: {'Content-Type': 'application/json'},
      ),
    );
    */
    if (response.statusCode == 200) {
      return response.data['name'];
    } else {
      throw ('Quien es ese pokemon?');
    }
  }
}
