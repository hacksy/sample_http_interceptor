import 'package:flutter/material.dart';
import 'package:sample_dio/pokemon_home.dart';

void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: PokemonHome(),
    );
  }
}