import 'package:flutter/material.dart';

import 'get_data.dart';

class PokemonHome extends StatefulWidget {
  @override
  _PokemonHomeState createState() => _PokemonHomeState();
}

typedef Future<String> GetPokemon();

class _PokemonHomeState extends State<PokemonHome> {
  String pokemonId = "";
  GetPokemon getPokemon = () async => '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PokeApi"),
      ),
      body: Column(
        children: [
          TextField(
            keyboardType: TextInputType.number,
            onChanged: (text) {
              setState(() => pokemonId = text);
            },
          ),
          RaisedButton(
            onPressed: () {
              getPokemon = () => GetData.fetchData(pokemonId);
              setState(() {});
            },
            child: Text("Get Pokemon"),
          ),
          FutureBuilder<String>(
            initialData: '',
            future: getPokemon(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Text(snapshot.data);
              }
              if (snapshot.hasError) {
                return Text("Quien es ese pokemon?");
              }
              return CircularProgressIndicator();
            },
          ),
        ],
      ),
    );
  }
}
