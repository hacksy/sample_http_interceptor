import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http_interceptor/http_interceptor.dart';

class GetData {
  static http.Client customClient =
      HttpClientWithInterceptor.build(interceptors: [
    EnvironmentInterceptor(),
    CleanHeadersInterceptor(),
    LogInterceptor(),
  ]);

  static const String BASE_URL = "https://pokeapi.co/api/v2/pokemon/";
  static Future<String> fetchData(String pokemonId) async {
    String url = '$BASE_URL$pokemonId';
    http.Response response = await customClient.get(url);
    if (response.statusCode == 200) {
      return jsonDecode(response.body)['name'];
    } else {
      throw ('Quien es ese pokemon?');
    }
  }
}

class EnvironmentInterceptor extends InterceptorContract {
  String environment = "DEV";//PROD
  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    if(environment == "DEV"){
        data.baseUrl = "https://pokeapi.co/api/v2${data.baseUrl}";
    }else if(environment == "PROD"){
        data.baseUrl = "https://pokeapi.co/api/v2${data.baseUrl}";
    }else{
       data.baseUrl = "https://pokeapi.co/api/v2${data.baseUrl}";
    }
    print(data);
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    return data;
  }
}

class CleanHeadersInterceptor extends InterceptorContract {
  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    final etag = data.headers['etag'];
    data.headers.clear();
    data.headers['etag'] = etag;
    return data;
  }
}

class LogInterceptor extends InterceptorContract {
  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    data.headers['apikey'] = 'miapikey';
    print("Request");
    print('url');
    print(data.url);
    print('Headers:');
    print(data.headers);
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    data.headers['apikey'] = '';
    print("Response");
    print('url');
    print(data.url);
    print('Headers:');
    print(data.headers);
    return data;
  }
}
